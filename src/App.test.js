import React from 'react';
import { shallow, mount, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import App, { Todo, TodoForm, useTodos } from './App';

configure({ adapter: new Adapter() })

describe('App', () => {
  describe('Todo', () => {
    it('run completeTodo when click Complete', () => {
      const completeTodo = jest.fn()
      const removeTodo = jest.fn()
      const index = 5;
      const todo = {
        text: 'one activity',
        isCompleted: false
      }

      const wrapper = shallow(<Todo
        todo={todo}
        index={index}
        completeTodo={completeTodo}
        removeTodo={removeTodo}
      />)

      wrapper
        .find('button')
        .at(0)
        .simulate('click')

      expect(completeTodo.mock.calls).toEqual([[5]])
      expect(removeTodo.mock.calls).toEqual([])
    });

    it('run removeTodo when click X', () => {
      const completeTodo = jest.fn()
      const removeTodo = jest.fn()
      const index = 5;
      const todo = {
        text: 'one activity',
        isCompleted: false
      }

      const wrapper = shallow(<Todo
        todo={todo}
        index={index}
        completeTodo={completeTodo}
        removeTodo={removeTodo}
      />)

      wrapper
        .find('button')
        .at(1)
        .simulate('click')

      expect(removeTodo.mock.calls).toEqual([[5]])
      expect(completeTodo.mock.calls).toEqual([])
    })
  })

  describe('TodoForm', () => {
    it('addTodo when handleSubmit', () => {
      const addTodo = jest.fn()
      const prevent = jest.fn()

      const wrapper = shallow(<TodoForm
        addTodo={addTodo}
      />)

      wrapper
        .find('input')
        .simulate('change', { target: { value: 'new todo item!' } })
      wrapper
        .find('form')
        .simulate('submit', { preventDefault: prevent })

        expect(addTodo.mock.calls).toEqual([[ 'new todo item!' ]])
        expect(prevent.mock.calls).toEqual([[]])
    })
  })

  describe('Custom hooks', () => {
    const Test = (props) => {
      const hook = props.hook()
      return <div { ...hook }/>
    }
    it('addTodo', () => {
      const wrapper = shallow(<Test hook={useTodos}/>)
      let props = wrapper.find('div').props()
      props.addTodo('Test text')
      props = wrapper.find('div').props()

      expect(props.todos[0]).toEqual({ text: 'Test text' })
    })
    it('completeTodo', () => {
      const wrapper = shallow(<Test hook={useTodos}/>)
      let props = wrapper.find('div').props()
      props.completeTodo(0)
      props = wrapper.find('div').props()

      expect(props.todos[0]).toEqual({ text: 'Todo 1', isCompleted: true })
    })
    it('removeTodo', () => {
      const wrapper = shallow(<Test hook={useTodos}/>)
      let props = wrapper.find('div').props()
      props.removeTodo(0)
      props = wrapper.find('div').props()

      expect(props.todos).toEqual([
        {
          text: "Todo 2",
          isCompleted: false
        },
        {
          text: "Todo 3",
          isCompleted: false
        }
      ])
    })
    it('App', () => {
      const wrapper = mount(<App />)
      const prevent = jest.fn()

      wrapper
        .find('input')
        .simulate('change', { target: { value: 'another item' } })
      wrapper
        .find('form')
        .simulate('submit', { preventDefault: prevent })

      const response = wrapper
                      .find('.todo')
                      .at(0)
                      .text()
                      .includes('another item')

      expect(response).toEqual(true)
      expect(prevent.mock.calls).toEqual([[]])
    })
  })
});
